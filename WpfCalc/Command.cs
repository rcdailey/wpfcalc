﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace WpfCalc
{
    class Command<T> : ICommand
        where T:IComparable
    {
        public Command(Action<T> execute)
        {
            _execute = execute;
        }

        public bool CanExecute(object parameter)
        {
            return true;
        }

        public event EventHandler CanExecuteChanged
        {
            add {}
            remove {}
        }

        public void Execute(object parameter)
        {
            _execute((T)Convert.ChangeType(parameter, typeof(T)));
        }

        private readonly Action<T> _execute;
    }
}
