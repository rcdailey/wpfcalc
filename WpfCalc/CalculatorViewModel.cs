﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using MvvmUtils;

namespace WpfCalc
{
    public class CalculatorViewModel : ViewModelBase
    {
        public float Result
        {
            get
            {
                return _current_number != 0 ? _current_number : _result;
            }
        }

        public CalculatorViewModel()
        {
            Clear();

            _operations = new Dictionary<string, Func<float, float, float>>();
            _operations.Add("+", (x, y) => x + y);
            _operations.Add("-", (x, y) => x - y);
            _operations.Add("/", (x, y) => x / y);
            _operations.Add("*", (x, y) => x * y);
        }

        private void Clear()
        {
            _current_op = null;
            _result = 0;
            _current_number = 0;
            OnPropertyChanged("Result");
        }

        [MvvmCommand]
        public void NumberButtonPressed(string number)
        {
            int digit = Int32.Parse(number);
            if (digit >= 0 && digit < 10) {
                _current_number = (_current_number * 10) + digit;
                OnPropertyChanged("Result");
            }
        }

        [MvvmCommand]
        public void OperatorButtonPressed(string operator_code)
        {
            _current_op = _operations[(string)operator_code];

            // This handles the case where on our first operation on the
            // calculator, the view does not show the 2nd number typed after
            // the operator key is pressed because we do not have a result yet.
            if (_result == 0) {
                _result = _current_number;
                _current_number = 0;
                OnPropertyChanged("Result");
            }
        }

        [MvvmCommand]
        public void EqualButtonPressed()
        {
            if (_current_op != null) {
                _result = _current_op( _result, _current_number );
                _current_op = null;
                _current_number = 0;
                OnPropertyChanged("Result");
            }
        }

        [MvvmCommand]
        public void ClearButtonPressed()
        {
            Clear();
        }
        
        float _result;
        float _current_number;
        Func<float, float, float> _current_op;
        Dictionary<string, Func<float, float, float>> _operations;
    }
}
