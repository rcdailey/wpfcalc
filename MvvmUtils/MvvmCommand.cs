﻿using System;
using System.Reflection;
using System.Windows.Input;

namespace MvvmUtils
{
    public class MvvmCommand : ICommand
    {
        public MvvmCommand(object instance, MethodInfo execute)
        {
            _instance = instance;
            _execute = execute;
        }

        public bool CanExecute(object parameter)
        {
            return true;
        }

        public event EventHandler CanExecuteChanged
        {
            add { }
            remove { }
        }

        public void Execute(object parameter)
        {
            object[] parameters = null;
            if (parameter != null) {
                if (parameter.GetType().IsArray) {
                    parameters = (object[])parameter;
                }
                else {
                    parameters = new object[] { parameter };
                }
            }

            _execute.Invoke(_instance, parameters);
        }

        private readonly MethodInfo _execute;
        private readonly object _instance;
    }
}
