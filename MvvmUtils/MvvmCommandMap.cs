﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Input;

namespace MvvmUtils
{
    [TypeDescriptionProvider(typeof(CommandMapDescriptionProvider))]
    public class MvvmCommandMap
    {
        public Dictionary<string, ICommand> Commands { get; set; }

        public MvvmCommandMap()
        {
            Commands = new Dictionary<string, ICommand>();
        }

        public void AddCommand(string key, ICommand cmd)
        {
            Commands[key] = cmd;
            NotifyCommandAdded(key, cmd);
        }

        public event Action<string, ICommand> NotifyCommandAdded = delegate { };
    }

    /// <summary>
    /// Expose the dictionary entries of a CommandMap as properties
    /// </summary>
    class CommandMapDescriptionProvider : TypeDescriptionProvider
    {
        /// <summary>
        /// Standard constructor
        /// </summary>
        public CommandMapDescriptionProvider()
            : this(TypeDescriptor.GetProvider(typeof(MvvmCommandMap)))
        {
        }

        /// <summary>
        /// Construct the provider based on a parent provider
        /// </summary>
        /// <param name="parent"></param>
        public CommandMapDescriptionProvider(TypeDescriptionProvider parent)
            : base(parent)
        {
        }

        /// <summary>
        /// Get the type descriptor for a given object instance
        /// </summary>
        /// <param name="objectType">The type of object for which a type descriptor is requested</param>
        /// <param name="instance">The instance of the object</param>
        /// <returns>A custom type descriptor</returns>
        public override ICustomTypeDescriptor GetTypeDescriptor(Type objectType, object instance)
        {
            return new CommandMapDescriptor(base.GetTypeDescriptor(objectType, instance), instance as MvvmCommandMap);
        }
    }

    /// <summary>
    /// This class is responsible for providing custom properties to WPF - in this instance
    /// allowing you to bind to commands by name
    /// </summary>
    class CommandMapDescriptor : CustomTypeDescriptor
    {
        /// <summary>
        /// Store the command map for later
        /// </summary>
        /// <param name="descriptor"></param>
        /// <param name="map"></param>
        public CommandMapDescriptor(ICustomTypeDescriptor descriptor, MvvmCommandMap map)
            : base(descriptor)
        {
            map.NotifyCommandAdded += CommandAdded;

            var props = new List<PropertyDescriptor>();

            foreach (KeyValuePair<string, ICommand> command in map.Commands)
                props.Add(new CommandPropertyDescriptor(command));
            
            _propCollection = new PropertyDescriptorCollection(props.ToArray());
        }

        private void CommandAdded(string key, ICommand cmd)
        {
            _propCollection.Add(new CommandPropertyDescriptor(
                new KeyValuePair<string, ICommand>(key, cmd)));
        }

        /// <summary>
        /// Get the properties for this command map
        /// </summary>
        /// <returns>A collection of synthesized property descriptors</returns>
        public override PropertyDescriptorCollection GetProperties()
        {
            return _propCollection;
        }

        private PropertyDescriptorCollection _propCollection;
    }

    /// <summary>
    /// A property descriptor which exposes an ICommand instance
    /// </summary>
    class CommandPropertyDescriptor : PropertyDescriptor
    {
        /// <summary>
        /// Construct the descriptor
        /// </summary>
        /// <param name="command"></param>
        public CommandPropertyDescriptor(KeyValuePair<string, ICommand> command)
            : base(command.Key, null)
        {
            _command = command.Value;
        }

        /// <summary>
        /// Always read only in this case
        /// </summary>
        public override bool IsReadOnly
        {
            get { return true; }
        }

        /// <summary>
        /// Nope, it's read only
        /// </summary>
        /// <param name="component"></param>
        /// <returns></returns>
        public override bool CanResetValue(object component)
        {
            return false;
        }

        /// <summary>
        /// Not needed
        /// </summary>
        public override Type ComponentType
        {
            get { throw new NotImplementedException(); }
        }

        /// <summary>
        /// Get the ICommand from the parent command map
        /// </summary>
        /// <param name="component"></param>
        /// <returns></returns>
        public override object GetValue(object component)
        {
            MvvmCommandMap map = component as MvvmCommandMap;

            if (null == map)
                throw new ArgumentException("component is not a CommandMap instance", "component");

            return map.Commands[this.Name];
        }

        /// <summary>
        /// Get the type of the property
        /// </summary>
        public override Type PropertyType
        {
            get { return typeof(ICommand); }
        }

        /// <summary>
        /// Not needed
        /// </summary>
        /// <param name="component"></param>
        public override void ResetValue(object component)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Not needed
        /// </summary>
        /// <param name="component"></param>
        /// <param name="value"></param>
        public override void SetValue(object component, object value)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Not needed
        /// </summary>
        /// <param name="component"></param>
        /// <returns></returns>
        public override bool ShouldSerializeValue(object component)
        {
            return false;
        }

        /// <summary>
        /// Store the command which will be executed
        /// </summary>
        private ICommand _command;
    }
}