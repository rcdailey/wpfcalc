﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Reflection;

namespace MvvmUtils
{
    public class MvvmCommandClass
    {
        private MvvmCommandMap _commandMap;
        public MvvmCommandMap Commands
        {
            get { return _commandMap; }
        }

        public MvvmCommandClass()
        {
            _commandMap = new MvvmCommandMap();
            Type derivedType = this.GetType();

            foreach (MethodInfo member in derivedType.GetMethods()) {
                if (member.GetCustomAttribute(typeof(MvvmCommandAttribute)) != null) {
                    _commandMap.AddCommand(member.Name, new MvvmCommand(this, member));
                }
            }
        }
    }
}
