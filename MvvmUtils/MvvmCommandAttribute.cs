﻿using System;
using System.Collections.Generic;

namespace MvvmUtils
{
    [AttributeUsage(AttributeTargets.Method)]
    public class MvvmCommandAttribute : System.Attribute
    {
    }
}
