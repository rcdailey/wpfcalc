﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MvvmUtils;

namespace ConsoleTest
{
    class Program
    {
        class TestModelView2 : MvvmCommandClass
        {
            [MvvmCommand]
            public void HandleButtonPress2(object parameter)
            {
                Console.WriteLine("HandleButtonPress2");
            }
        }

        class TestModelView : TestModelView2
        {
            [MvvmCommand]
            public void HandleButtonPress(object parameter)
            {
                Console.WriteLine("HandleButtonPress");
            }
        }

        static void Main(string[] args)
        {
            TestModelView test = new TestModelView();

            var d = new Dictionary<int, string>();
            d[0] = "test";
            KeyValuePair<int,string>[] kvp = d.ToArray();
        }
    }
}
